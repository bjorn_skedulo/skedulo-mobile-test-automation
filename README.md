# Proof of concept / Kotlin, Appium, Cucumber on AWS Device farm

This is a poc of the combination of using Kotlin, Appium and Cucumber and run tests of Mobile App on AWS Device farm

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

The project requires that you have the following installed:

```
Maven, node, java, Appium, Android studio
```

### Installing



Clone the project

```
git clone git@bitbucket.org:bjorn_skedulo/skedulo-mobile-test-automation.git
cd skedulo-mobile-test-automation
```

get the correct branch

```
git fetch && git checkout CucumberKotlinAWS
```

The project is a very basic test that will login a user on the app and verify that the user is logged in.

## Running the tests

You can run the tests on you local environment and in AWS Device farm

### Run tests on you local environment

Before you run tests on your local start an emulator
Use maven to trigger the tests

```
mvn clean package
```

### To run on AWS device farm

In order to run on AWS device farm you have to create a zip file to upload to AWS

```
mvn clean package -DskipTests
```

## Deployment

Tbd

## Built With

* [Appium](http://www.http://appium.io/) - The mobile automation tool
* [Cucumber](https://cucumber.io/) - The bdd test automation tool
* [Maven](https://maven.apache.org/) - Dependency Management
* [Kotlin](https://kotlinlang.org/) - Programming language


## Contributing


## Versioning


## Authors


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments


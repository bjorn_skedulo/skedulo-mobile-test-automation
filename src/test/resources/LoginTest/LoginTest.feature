
Feature: Login

  @POC
  Scenario Outline: Login flow via app

  As a user I should be able to Login with my Email and Password

    Given I navigate to the login page
    When I choose to login with salesforce
    And I fill in email with "<login>"
    And I fill in password with "<password>"
    And I click on the Login button
    Then I should be on the welcome page

    Examples:
      |       login              |   password    |
      |xtest1@skedulo.com|54321login   |


#    Given I navigate to the login page
#    When I choose to login with "<alternative>
#    And I login with email with "<login>" and "<password>"
#    Then I should be on the welcome page
#
#    Examples:
#      |       alternative | login              |   password    | result |
#      | salesforce        | xtest1@skedulo.com |54321login   | pass     |
#      | salesforce        | fake@skedulo.com   |54321login   | fail     |
#      | salesforce        | xtest1@skedulo.com | wrongpwd    | fail     |

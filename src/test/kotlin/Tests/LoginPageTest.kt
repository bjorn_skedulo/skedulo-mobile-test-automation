package Tests

import Pages.LoginPage
import Pages.SalesForceLoginPage
import Pages.HomePage
import Tests.AbstractBaseTests.TestBase
import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.testng.Assert
import kotlin.test.assertTrue


@CucumberOptions(strict = true, monochrome = true, features = ["classpath:LoginTest"], plugin = ["pretty"])
open class LoginPageTest() : TestBase() {
    private var loginPage: LoginPage? = null
    private var sfLogin: SalesForceLoginPage? = null

    /**
     * Creates a login
     */
    @Given("^I navigate to the login page$")
    override fun setUpPage() {

        loginPage = LoginPage(TestBase.Companion.getAndroidDriver())
        assertTrue(loginPage?.isPageLoaded!!, "Login page is not loaded !")
    }

    @When("^I choose to login with salesforce$")
    @Throws(Throwable::class)
    fun i_choose_to_login_with_salesforce() {
        this.loginPage?.clickSignInWithSalesForce()


    }

    @When("^I fill in email with \"([^\"]*)\"$")
    @Throws(Throwable::class)
    fun i_fill_in_email_with(email: String) {
        sfLogin = SalesForceLoginPage(TestBase.Companion.getAndroidDriver())
        assertTrue(this.sfLogin?.isPageLoaded!!, "Page is not loaded !")
        this.sfLogin!!.setEmail(email)


    }

    @When("^I fill in password with \"([^\"]*)\"$")
    @Throws(Throwable::class)
    fun i_fill_in_password_with(password: String) {
        sfLogin!!.setPassword(password)

    }

    @When("^I click on the Login button$")
    @Throws(Throwable::class)
    fun i_click_on_the_Login_button() {
        sfLogin!!.clickLogin()

    }

    @Then("^I should be on the welcome page$")
    @Throws(Throwable::class)
    fun i_should_be_on_the_welcome_page() {
        val home = HomePage(TestBase.Companion.getAndroidDriver())
        Assert.assertTrue(home.isPageLoaded, "Page is not loaded !")
        home.clickMenu()

    }

}

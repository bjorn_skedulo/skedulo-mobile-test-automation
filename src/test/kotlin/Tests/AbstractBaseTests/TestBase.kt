/*
 * Copyright 2014-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 * http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package Tests.AbstractBaseTests

import Pages.ConfirmLogoutPage
import Pages.MenuPage
import cucumber.api.testng.AbstractTestNGCucumberTests
import io.appium.java_client.MobileElement
import io.appium.java_client.android.AndroidDriver
import org.openqa.selenium.remote.DesiredCapabilities
import org.testng.annotations.*

import java.net.MalformedURLException
import java.net.URL
import java.util.concurrent.TimeUnit

/**
 * An abstract base for all of the Android tests within this package
 *
 * Responsible for setting up the Appium test Driver
 */
abstract class TestBase : AbstractTestNGCucumberTests() {

    /**
     * Method to initialize the test's page
     */
    @BeforeTest
    abstract fun setUpPage()

    /**
     * This method runs after each test in order to logout from the app
     */
    @AfterMethod
    fun tearDown() {
        val menu = MenuPage(driver)
        menu.isPageLoaded
        menu.swipeToBottom()
        menu.clickLogOut()
        val confirm = ConfirmLogoutPage(driver)
        confirm.isPageLoaded
        confirm.clickLogout()

    }

    /**
     * This method runs before any other method.
     *
     * Appium follows a client - server model:
     * We are setting up our appium client in order to connect to Device Farm's appium server.
     *
     * We do not need to and SHOULD NOT set our own DesiredCapabilities
     * Device Farm creates custom settings at the server level. Setting your own DesiredCapabilities
     * will result in unexpected results and failures.
     *
     * @throws MalformedURLException An exception that occurs when the URL is wrong
     */
    @BeforeSuite
    @Throws(MalformedURLException::class)
    fun setUpAppium() {

        val URL_STRING = "http://127.0.0.1:4723/wd/hub"

        val url = URL(URL_STRING)

        val capabilities = DesiredCapabilities()

        //Set the DesiredCapabilities capabilities only for local development
        capabilities.setCapability("platformName", "Android")
        capabilities.setCapability("deviceName", "Android Emulator")
        capabilities.setCapability("appActivity", "md50051b690d572208ed6ae8ef2f5987806.LaunchScreen")
        capabilities.setCapability("appPackage", "com.skedulo.app")
        capabilities.setCapability("unicodeKeyboard", true)
        capabilities.setCapability("resetKeyboard", true)
        capabilities.setCapability("autoGrantPermissions", true)

        //capabilities.setCapability("app", "/Users/bjornandersson/downloads/mobileApp/com.skedulo.app-Signed.apk");
        //capabilities.setCapability("udid", "emulator-5554");


        driver = AndroidDriver(url, capabilities)

        //Use a higher value if your mobile elements take time to show up
        driver.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS)
    }

    /**
     * Always remember to quit
     */
    @AfterSuite
    fun tearDownAppium() {
        driver.quit()
    }


    /**
     * Restart the app after every test class to go back to the main
     * screen and to reset the behavior
     */
    @AfterClass
    fun restartApp() {
        driver.resetApp()
    }

    companion object {
        init{
            println("Singelton class invoked")
        }
        private lateinit var driver: AndroidDriver<MobileElement>

        fun getAndroidDriver() = driver
    }
}
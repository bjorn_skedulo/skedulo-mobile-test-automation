package Pages


import io.appium.java_client.AppiumDriver
import io.appium.java_client.android.AndroidElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSBy
import org.openqa.selenium.support.PageFactory


class HomePage(driver: AppiumDriver<*>) : BasePage(driver) {

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Skedulo\"]")
    private val Skedulo: AndroidElement? = null

    @iOSBy(accessibility = "")
    @AndroidFindBy(xpath = "//*[@text=\"TODAY\"]")
    private val TodayBtn: AndroidElement? = null

    @iOSBy(accessibility = "")
    @AndroidFindBy(xpath = "//*[@text=\"WEEK\"]")
    private val WeekBtn: AndroidElement? = null

    @iOSBy(accessibility = "")
    @AndroidFindBy(xpath = "//*[@text=\"MONTH\"]")
    private val MonthBtn: AndroidElement? = null

    @iOSBy(accessibility = "")
    @AndroidFindBy(xpath = "//*[@contentDescription=\"Add\"]")
    private val AddBtn: AndroidElement? = null

    @iOSBy(accessibility = "")
    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc=\"OK\"]")
    private val MenuBtn: AndroidElement? = null

    @iOSBy(accessibility = "")
    @AndroidFindBy(xpath = "android.widget.TextView[@content-desc=\"LabelUserWelcome\"]")
    private val UserWelcome: AndroidElement? = null

    @iOSBy(accessibility = "")
    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"LabelUserAvailability\"]")
    private val UserAvailability: AndroidElement? = null

    @iOSBy(accessibility = "")
    @AndroidFindBy(xpath = "//*[@text=\"No jobs or activities\"]")
    private val NoJobsOrActivities: AndroidElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }
    //Getters and setters
    val isPageLoaded: Boolean
        get() = Skedulo!!.isDisplayed

    fun clickMenu() {
        MenuBtn!!.click()
    }


}


package Pages


import io.appium.java_client.AppiumDriver
import io.appium.java_client.android.AndroidElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSBy
import org.openqa.selenium.support.PageFactory

class MenuPage(driver: AppiumDriver<*>) : BasePage(driver) {

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Agenda\"]")
    private val AgendaBtn: AndroidElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Unavailability\"]")
    private val UnavailabilityBtn: AndroidElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Social Feed\"]")
    private val SocialFeedBtn: AndroidElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Notifications\"]")
    private val NotificationsBtn: AndroidElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Offers\"]")
    private val OffersBtn: AndroidElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Settings\"]")
    private val SettingsBtn: AndroidElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Log Out\"]")
    private val LogOutBtn: AndroidElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }

    val isPageLoaded: Boolean
        get() = AgendaBtn!!.isDisplayed

    fun clickLogOut() {
        LogOutBtn!!.click()
    }

    fun swipeToBottom() {
        swipeByCordinates(442, 512, 442, 10)
    }

}

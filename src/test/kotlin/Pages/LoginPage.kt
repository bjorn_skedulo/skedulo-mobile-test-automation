package Pages


import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSBy
import org.openqa.selenium.By
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.PageFactory
import org.openqa.selenium.support.pagefactory.AjaxElementLocator
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory
import kotlin.test.assertFails

class LoginPage(driver: AppiumDriver<*>) : BasePage(driver) {

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text='Log in with Salesforce']")
    private val btnSignInWithSalesForce: MobileElement? = null

    @AndroidFindBy(xpath = "//*[@text='Log in with team name']")
    private val btnSignInWithTeamName: MobileElement? = null

    @AndroidFindBy(xpath = "//*[@text='More options']")
    private val btnMoreOptions: MobileElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }
    val isPageLoaded: Boolean?
        get() {
            val loginElement = driver.findElement(By.xpath("//*[@text='Log in with Salesforce']"))
            return loginElement.isDisplayed
        }

    fun clickSignInWithSalesForce() {
        btnSignInWithSalesForce?.click()
    }

    fun clickSignInWithTeamName() {

        btnSignInWithTeamName!!.click()

    }

    fun clickMoreOptions() {

        btnMoreOptions!!.click()
    }

}
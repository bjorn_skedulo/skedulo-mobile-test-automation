/*
 * Copyright 2014-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 * http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package Pages


import io.appium.java_client.AppiumDriver
import io.appium.java_client.TouchAction
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.PageFactory

import io.appium.java_client.touch.WaitOptions.waitOptions
import io.appium.java_client.touch.offset.PointOption.point
import util.PlatformTouchAction
import java.time.Duration.ofMillis


/**
 * A base for all the pages within the suite
 */
abstract class BasePage
/**
 * A base constructor that sets the page's driver
 *
 * The page structure is being used within this test in order to separate the
 * page actions from the tests.
 *
 * Please use the AppiumFieldDecorator class within the page factory. This way annotations
 * like @AndroidFindBy within the page objects.
 *
 * @param driver the appium driver created in the beforesuite method.
 */
protected constructor(
        /**
         * The driver
         */
        protected val driver: AppiumDriver<*>) {

    init {
        //PageFactory.initElements(new AppiumFieldDecorator(driver, 5, TimeUnit.SECONDS), this);
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }

    /**
     * Tries three times to send text to element properly.
     *
     * Note: This method was needed because Appium sometimes sends text to textboxes incorrectly.
     *
     * @param input String to be sent
     * @param element WebElement to receive text, cannot be a secure text field.
     * @param appendNewLine true to append a new line character to incoming string when sending to element, else false
     *
     * @return true if keys were successfully sent, otherwise false.
     */
    @Throws(InterruptedException::class)
    protected fun sendKeysToElement(input: String, element: WebElement, appendNewLine: Boolean): Boolean {
        val MAX_ATTEMPTS = 3
        var attempts = 0

        do {
            element.clear()
            Thread.sleep(KEYBOARD_ANIMATION_DELAY.toLong())

            if (appendNewLine) {
                element.sendKeys(input + "\n")
            } else {
                element.sendKeys(input)
            }

            Thread.sleep(XML_REFRESH_DELAY.toLong())
        } while (!element.text.contains(input) && ++attempts < MAX_ATTEMPTS)

        return element.text.contains(input)
    }

    //    public void assertOnPage(long timeOutInSeconds) {
    //        String message = String.format(
    //                "Unable to verify on page: %s. Waited for '%s'",
    //                this.getClass().getSimpleName(),
    //                trait().getCurrent().toString()
    //        );
    //
    //        wait
    //                .withTimeout(timeOutInSeconds, TimeUnit.SECONDS)
    //                .withMessage(message)
    //                .until(ExpectedConditions.presenceOfElementLocated(trait().getCurrent()));
    //    }
    fun swipeByCordinates(startX: Int, startY: Int, endX: Int, endY: Int) {

        PlatformTouchAction(driver)
                .press(point(startX, startY))
                .waitAction(waitOptions(ofMillis(5000)))
                .moveTo(point(endX, endY))
                .release()
                .perform()
    }

    companion object {
        private val KEYBOARD_ANIMATION_DELAY = 1000
        private val XML_REFRESH_DELAY = 1000
    }
}
package Pages

import io.appium.java_client.AppiumDriver
import io.appium.java_client.android.AndroidElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSBy
import org.openqa.selenium.support.PageFactory

class ConfirmLogoutPage(driver: AppiumDriver<*>) : BasePage(driver) {

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(id = "button1")
    private val LogOutBtn: AndroidElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(id = "button2")
    private val CancelBtn: AndroidElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Confirm Log Out\"]")
    private val PageTitle: AndroidElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Logging out will remove all local data\"]")
    private val PageText: AndroidElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }
    val pageTitle: String
        get() = PageTitle!!.text
    val pageText: String
        get() = PageText!!.text
    val isPageLoaded: Boolean
        get() = PageTitle!!.isDisplayed

    fun clickLogout() {
        LogOutBtn!!.click()
    }

    fun clickCancel() {
        CancelBtn!!.click()
    }

}

package Pages


import io.appium.java_client.AppiumDriver
import io.appium.java_client.android.AndroidElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSBy
import org.openqa.selenium.support.PageFactory


class TeamNameLoginPage(driver: AppiumDriver<*>) : BasePage(driver) {

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Enter team name\"]")
    private val enterTeamNameField: AndroidElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"More options\"]")
    private val moreOptionsBtn: AndroidElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Next\"]")
    private val nextBtn: AndroidElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@text=\"Forgot?\"]")
    private val forgotBtn: AndroidElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(xpath = "//*[@class='android.widget.ImageButton']")
    private val backBtn: AndroidElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }

    fun setTeamName(teamName: String) {
        enterTeamNameField!!.sendKeys(teamName)
    }

    fun clickNext() {
        nextBtn!!.click()
    }

    fun clickForgot() {
        forgotBtn!!.click()
    }

}

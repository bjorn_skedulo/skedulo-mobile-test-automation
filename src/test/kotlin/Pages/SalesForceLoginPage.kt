package Pages

import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import io.appium.java_client.pagefactory.iOSBy
import org.openqa.selenium.support.PageFactory

class SalesForceLoginPage(driver: AppiumDriver<*>) : BasePage(driver) {

    @iOSBy(accessibility = "plus")
    //@AndroidFindBy(xpath = "//*[@resource-id=\"username_container\"]")
    @AndroidFindBy(id = "username")
    private val emailField: MobileElement? = null

    @iOSBy(accessibility = "")
    @AndroidFindBy(id = "idcard-identity")
    private val idcard: MobileElement? = null

    @iOSBy(accessibility = "")
    @AndroidFindBy(id = "clear_link")
    private val clearUserName: MobileElement? = null

    @iOSBy(accessibility = "")
    @AndroidFindBy(id = "hint_back_chooser")
    private val savedUserNames: MobileElement? = null

    @iOSBy(accessibility = "plus")
    @AndroidFindBy(id = "password")
    private val passField: MobileElement? = null

    @AndroidFindBy(id = "theloginform")
    private val theLoginForm: MobileElement? = null

    @iOSBy(id = "")
    @AndroidFindBy(id = "Login")
    private val loginBtn: MobileElement? = null

    //@iOSBy(id="")
    @AndroidFindBy(id = "rememberUn")
    private val rememberMe: MobileElement? = null

    //@iOSBy(id="")
    @AndroidFindBy(id = "close_button")
    private val closeBtn: MobileElement? = null

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }

    val isPageLoaded: Boolean
        get() = passField!!.isDisplayed

    fun setEmail(email: String) {

        emailField!!.sendKeys(email)
        swipeByCordinates(422, 1046, 469, 434)

    }

    fun setPassword(password: String) {
        passField!!.click()
        if (passField.text != password) {
            passField.clear()
            passField.sendKeys(password)
        }
    }

    fun clickLogin() {
        loginBtn!!.click()
    }

    fun clickLoginForm() {
        theLoginForm!!.click()
    }

    fun clickRememberMe() {
        rememberMe!!.click()
    }

    fun clickCloseButton() {
        closeBtn!!.click()
    }

}
